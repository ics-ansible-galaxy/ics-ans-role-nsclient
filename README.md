ics-ans-role-nsclient
===================

Ansible role to configure the resolv.conf files on our hosts in the technical
network, and sometimes also the ICS lab network.

All values are preconfigured in the role, just choose which network the host
or group resides on.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

Standard value for hosts / groups on the technical network. This value is implicit.
```yaml
nsclient_network_segment: "tn"
```

If host or group belong to ICS LAB, set below to "icslab" in host/group_vars
```yaml
nsclient_network_segment: "icslab"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-nsclient
```

License
-------

BSD 2-clause
