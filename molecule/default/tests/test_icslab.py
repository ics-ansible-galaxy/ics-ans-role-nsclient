import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('icslab_group')


def test_nsclient_icslab_subnet(nsclient_resolv):
    assert nsclient_resolv == [u'# Options',
                               u'',
                               u'# Nameservers',
                               u'nameserver 172.16.6.10',
                               u'',
                               u'# Search',
                               u'search ics.esss.lu.se']
