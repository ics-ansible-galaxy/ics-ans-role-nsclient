import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_nsclient_technical_subnet(host):
    content = host.file('/etc/sysconfig/network-scripts/ifcfg-test').content
    assert b'PEERDNS=no' in content
