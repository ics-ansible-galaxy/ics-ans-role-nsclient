import pytest


@pytest.fixture(scope='module')
def nsclient_resolv(host):
    """Fixture that returns content of the /etc/resolv.conf as list
    """
    return host.file('/etc/resolv.conf').content_string.splitlines()
