import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('default_group')


def test_nsclient_technical_subnet(nsclient_resolv):
    assert nsclient_resolv == [u'# Options',
                               u'options rotate',
                               u'options timeout:2',
                               u'',
                               u'# Nameservers',
                               u'nameserver 172.16.6.21',
                               u'nameserver 172.16.6.22',
                               u'',
                               u'# Search',
                               u'search tn.esss.lu.se ics.esss.lu.se']
